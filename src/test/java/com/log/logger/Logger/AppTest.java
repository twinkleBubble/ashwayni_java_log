package com.log.logger.Logger;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

/**
 * Unit test for simple App.
 */
public class AppTest {

	@InjectMocks
	private App appObject;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGenerateLogFile() throws IOException {
		appObject.generateLogFile();
	}

	@Test
	public void testRunErrorLogGenerator() {
		boolean status = appObject.runErrorLogGenerator("log4j.log");

	}

}
