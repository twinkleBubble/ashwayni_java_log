package com.log.logger.Logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

public class App {
	static Logger log = Logger.getLogger(App.class.getName());

	public static void generateLogFile() throws IOException {
		MDC.put("sessionId", 123);
		log.info("User3 logs in");
		MDC.put("sessionId", 120);
		log.info("User1 logs in");
		log.info("User1 goes to search page");
		log.info("User1 types in search text");

		MDC.put("sessionId", 121);
		log.info("User2 logs in");

		MDC.put("sessionId", 123);
		log.info("User3 runs some job");
		log.error("ERROR: Some Exception occurred");

		MDC.put("sessionId", 120);
		log.info("User1 clicks search button");

		MDC.put("sessionId", 121);
		log.info("User2 does something");
		log.error("ERROR: Some Exception occurred");

		MDC.put("sessionId", 120);
		log.info("User1 logs off");
		log.error("ERROR: Invalid input");

	}

	public boolean runErrorLogGenerator(String file) {
		try {
			File myObj = new File(file);
			Scanner myReader = new Scanner(myObj);
			List<String> keysOrder = new ArrayList<String>();
			Map<String, List<String>> map = new HashMap<String, List<String>>();
			while (myReader.hasNextLine()) {
				String line = myReader.nextLine();
				String dataInfo[] = line.split(" ");
				if (!keysOrder.contains(dataInfo[2])) {
					keysOrder.add(dataInfo[2]);
					List<String> lineList = new ArrayList<String>();
					lineList.add(line);
					map.put(dataInfo[2], lineList);
				} else {
					map.get(dataInfo[2]).add(line);
				}
			}

			if (!keysOrder.isEmpty()) {

				for (String key : keysOrder) {
					List<String> finalList = new ArrayList<String>();
					List<String> lineList = map.get(key);
					for (String line : lineList) {
						finalList.add(line);
						if (line.split(" ")[3].contains("ERROR")) {
							writeInFile(line, finalList);
							break;
						}
					}
				}

			}

			myReader.close();
			return true;
		} catch (FileNotFoundException e) {
			System.err.println("An error occurred.");
			e.printStackTrace();
		}
		return false;
	}

	public void writeInFile(String str, List<String> list) {
		try {
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("error.txt", true));

			int listSize = list.size();

			if (!list.isEmpty()) {
				for (int i = (listSize - 4); i < listSize; i++) {
					if (i > -1 && !(list.get(i).contains("ERROR"))) {
						bufferedWriter.write(list.get(i) + "\n");
					}
				}
			}

			bufferedWriter.write(str + "\n ------- \n");
			bufferedWriter.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
